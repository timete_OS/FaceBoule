import unittest

import no_app
import schemas


florent = schemas.UserCreate(
    email="flo.zittoun@eleve.ensai.fr",
    first_name="Florent",
    last_name="Zittoun",
    pswd="meilleure colloc",
)

timo = schemas.UserCreate(
    email="timo.bidouille@eleve.ensai.fr",
    first_name="Timo",
    last_name="Bidouille",
    pswd="coin coin",
)


class TestUserCreation(unittest.IsolatedAsyncioTestCase):

    async def base_test(self):
        response = await no_app.create_user(florent)
        self.assertIsInstance(response, schemas.UserBase)

    async def test_user_already_exist(self):
        response = await no_app.create_user(timo)
        print(type(response))
        self.assertIsInstance(response, no_app.HTTPException)


if __name__ == '__main__':
    unittest.main()
