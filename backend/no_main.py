import uvicorn
import os

from dotenv import load_dotenv
from no_app import app

load_dotenv()
port = int(os.environ.get("PORT"))
host = os.environ.get("HOST")

if __name__ == "__main__":
    uvicorn.run(app, host=host, port=port)
