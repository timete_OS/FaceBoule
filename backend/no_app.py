import logging
import schemas
import hashlib
import secrets
import random
import datetime
import os

# import dateutil.parser
from typing import Dict, List, Union
from dotenv import load_dotenv

from fastapi import Depends, FastAPI, HTTPException, Header, status
from fastapi.encoders import jsonable_encoder
from fastapi.middleware.cors import CORSMiddleware
from fastapi_utils.tasks import repeat_every

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy.sql import case

logging.basicConfig(filename='infos.log', encoding='utf-8', level=logging.INFO)


Base = declarative_base()

load_dotenv()
SQLALCHEMY_DATABASE_URL = os.environ.get("DATABASE_URL")

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, echo=False, connect_args={"check_same_thread": False, }
)

from mail import Mail
from game import Game
from user import User, Token
from orm import ORM

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


# creation of tables
Base.metadata.create_all(bind=engine)

db = ORM()
app = FastAPI()
mailing = Mail()

origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def get_session():
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()


# check if token is valid
def token_to_user(token):
    if token not in user_table:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"}
        )
    return user_table[token]


def upcast(object):
    """ useful in order not to return secret attributes of Users
    We do want to upcast some members of User, and since Python do not
    offer any way to do so, we have to :
    - retrieve the attributes we actualy want to use -> schemas.UserBase.__fields__.keys()
    - create new objects from those attributes

    I hoped super() and __new__() would help use in our quest, but they didn't

    Args:
        object (Game, User): 

    Returns:
        _type_: a parent object
    """
    if isinstance(object, Game):
        attrs = vars(object)
        attrs["winner"], attrs["looser"] = upcast(
            object.winner), upcast(object.looser)
        wanted_attr = schemas.GameBase.__fields__.keys()
        return schemas.GameBase(**attrs)
    if isinstance(object, User):
        wanted_attr = schemas.UserBase.__fields__.keys()
        return schemas.UserBase(**{attr: getattr(object, attr) for attr in wanted_attr})

def populate(session=SessionLocal()) -> None:
    db.populate(session)
    session.close()

def create_user_table(session=SessionLocal()):
    """Create a table of known tokens and their associated users
    Allows for quick verification of requests validity.

    Args:
        session (_type_, optional): Defaults to SessionLocal().
    """
    global user_table
    # ici il me faut une fonction qui cast un User vers un UserBase
    user_table = {
        token.value: upcast(db.get_user(session, token.owner_id))
        for token in db.get_all(Token, session)}
    print("ELO WORLD")
    session.close()

def decrease_elo(session=SessionLocal()):
    inactivity_days = int(os.environ.get("INACTIVITY_DAYS"))
    # decrease_factor = os.environ.get("DECREASE_FACTOR")
    max_games_in_a_day = 100 # ?
    recent_games = db.get_all(Game, session, skip=0, limit=14*max_games_in_a_day, order=Game.date)
    users = set(db.get_all(User, session, limit=1000))
    active_users = set()
    user_mapper = dict()
    limit = datetime.datetime.now() - datetime.timedelta(inactivity_days)

    i = 0
    print(f"la date de la partie est {recent_games[i].date}, et {recent_games[i].date > limit}")
    while (recent_games[i].date > limit) and (i < len(users)):
        game = recent_games[i]
        active_users = active_users.union({game.winner, game.looser})
        i += 1
    print(active_users)
    for user in users.difference(active_users):
        user.elo *= 0.99
        user_mapper[user.id] = user.elo
    # mise à jour des elos dans la base
    if user_mapper:
        session.query(User) \
            .filter(User.id.in_(user_mapper.keys())) \
            .update({
                User.elo: case(
                    user_mapper,
                    value=User.id
                )
            })
    session.commit()
    session.close()


@app.on_event("startup")
@repeat_every(seconds=60 * 60 * 24)  # 1 day
def periodic() -> None:
    # populate()
    create_user_table()
    decrease_elo()



@app.get("/")
def root():
    return {"True": False}


@app.put("/user")
async def create_user(user_creation: schemas.UserCreate, session: Session = Depends(get_session)):
    # here need to lowercase email
    user_creation.email = user_creation.email.lower()
    db_user = db.get_with(session, User, email=user_creation.email,
                          last_name=user_creation.last_name)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already registered")
    code = ''.join([str(random.randint(0, 9)) for _ in range(6)])
    t = datetime.datetime.now()
    user_table[code] = (t, user_creation)
    mailing.send(user_creation, code)
    logging.info(f'VOILA LE CODE DE {user_creation.first_name}: {code}')
    print(f"\n\n VOILA LE CODE DE {user_creation.first_name}: {code}\n")
    return True


@app.put("/user/email_confirmation")
async def confirm_email(mail: str, code: str, session: Session = Depends(get_session)):
    t, user_create = user_table.pop(code, (None, None))
    if user_create is None:
        return {"access_token": None, "valid_auth": False}
    if (datetime.datetime.now() - t).total_seconds() > 60*60*2:
        return "too late"
    if mail == user_create.email:
        value = secrets.token_hex(
            32) + hashlib.sha256(user_create.pswd.encode()).hexdigest()

        user = db.create_user(session, user_creation=user_create)
        db.create_token(session, Token(
            value=value,
            owner_id=user.id)
        )
        new_user = upcast(db.get_user(session, user.id))
        user_table[value] = new_user
        return {"access_token": value, "valid_auth": True, 'id' : new_user.id}
    return {"access_token": None, "valid_auth": False}


@app.get("/login")
async def login(email: str, pswd: str, session: Session = Depends(get_session)):
    email = email.lower()
    pswd = pswd.encode()
    m = hashlib.sha256(pswd).hexdigest()
    L = db.get_with(session, User, email=email, hashed_password=m)
    if L:
        user = L[0]
        # pas sur de capter ici
        # mais il faut signer le token avec le mdp ?
        # je pense que l'idée c'est surtout d'update le token avec le hash du mdp
        # à chaque requête, pour qu'un gars au milieu ne puisse rien faire
        value = secrets.token_hex(32) + hashlib.sha256(pswd).hexdigest()
        db.create_token(session, Token(
            value=value,
            owner_id=user.id)
        )
        new_user = upcast(db.get_user(session, user.id))
        user_table[value] = new_user
        return {"access_token": value, "valid_auth": True, 'id' : new_user.id}
    return {"access_token": None, "valid_auth": False}


@app.get("/users/{user_id}", response_model=schemas.User)
async def read_user(user_id: str, session: Session = Depends(get_session)):
    db_user = db.get_by_id(session, User, user_id)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user


@app.get("/users", response_model=List[schemas.User])
async def read_users(skip: int = 0, limit: int = 100, session: Session = Depends(get_session)):
    users = db.get_all(User, session, skip=skip, limit=limit)
    return users


@app.post("/user")
async def update_user(first_name:str, last_name:str, pseudo: str, token: Union[str, None] = Header(default=None), session: Session = Depends(get_session)):
    user = token_to_user(token)
    user.pseudo = pseudo
    user.first_name = first_name
    user.last_name = last_name
    session.query(User).\
        filter(User.id == user.id).\
        update({'pseudo': pseudo,
                'first_name' : first_name,
                'last_name' : last_name})
    session.commit()
    return user


@app.get("/who_am_I")
async def who_am_I(token: Union[str, None] = Header(default=None), session: Session = Depends(get_session)):
    user = token_to_user(token)
    return user


@app.put("/game", response_model=schemas.GameBase)
async def create_game(game: schemas.GameCreate,
                      token: Union[str, None] = Header(default=None),
                      session: Session = Depends(get_session)):
    user = token_to_user(token)
    # il convertit tout seul les dates, trop cool
    """ if game.date != '':
        game.date = dateutil.parser.isoparse(game.date)
    else:
        game.date = datetime.now() """
    if game.winner.id == game.looser.id:
        return "joue pas au malin"
    if game.looser.id == user.id:
        game.looser_validation = True
    if game.winner.id == user.id:
        game.winner_validation = True
    game = db.create_game(session, game)
    if game is None:
        return HTTPException(
            status_code=400,
            detail="No match for neither winner nor looser"
        )
    return upcast(game)


@app.post("/game/{game_id}/validation/")
async def validate_game_from_user(
    game_id: str,
    validation: bool,
    token: Union[str, None] = Header(default=None),
    session: Session = Depends(get_session)
):
    user = token_to_user(token)
    db_game = db.get_by_id(session, Game, game_id)
    game = upcast(db_game)
    session.commit()

    if game is None:
        raise HTTPException(status_code=404, detail="Game not found")

    if not(validation):
        print("IL FAUT ME SUPPRIMER \n")
        session.query(Game).filter(Game.id == game_id).delete()
        session.commit()
        return {'status' : 'deleted'}
    if not(game.winner_validation) and game.winner.id == user.id:
        db_game.winner_validation = validation
    if not(game.looser_validation) and game.looser.id == user.id:
        db_game.looser_validation = validation

    db_game.is_valid = (
        db_game.looser_validation and db_game.winner_validation)
    session.commit()
    if db_game.is_valid:
        print("JE SUIS BIEN VALIDE !\n")
        db_game.update_all_elo(session)
        # update database
    return {'status' : 'accepted'}


@app.get("/games", response_model=List[schemas.GameBase])
async def read_games(skip: int = 0, limit: int = 100, session: Session = Depends(get_session)):
    games = db.get_all(Game, session, skip=skip, limit=limit, order=Game.date)
    return [upcast(game) for game in games if game.is_valid]


@app.get("/games/{user_id}", response_model=List[schemas.GameBase])
async def game_from_user(user_id: str, session: Session = Depends(get_session)):
    return [upcast(game) for game in db.get_games_from_user(session, user_id) if (game.is_valid)]



# I can't return {User : {'total' : , 'victory': , 'defeat': }}, such a shame
@app.get("/{user_id}/duels")#, response_model=Dict[schemas.User, dict])#{schemas.UserBase : Dict{str : int}})
async def duels(user_id:str, session: Session = Depends(get_session)):
    score = dict()
    games = await game_from_user(user_id, session)
    for game in games:
        b = (game.winner.id == user_id)
        id = game.looser.id*b + game.winner.id*(1 - b) 
        if id not in score.keys():
            user = game.winner
            if b:
                user = game.looser
            score[id] = jsonable_encoder(user)
            score[id]['total_games'] = 0
            score[id]['victory'] = 0
            score[id]['defeat'] = 0

        score[id]['total_games'] += 1
        score[id]['victory'] += + b*1
        score[id]['defeat'] += not(b)*1

    return list(score.values())

@app.get("/games/{user_id}/unconfirmed", response_model=List[schemas.GameBase])
async def unconfirmed_game_from_user(user_id: str, session: Session = Depends(get_session)):
    games = db.get_games_from_user(session, user_id)
    # readeabeality xxxdlol
    L = [upcast(game) for game in games if
         (((game.winner_id == user_id) and (game.winner_validation == False))
             or
             ((game.looser_id == user_id) and (game.looser_validation == False)))
         ]
    return L



@app.get("/refresh_all_elo")
async def refresh_all_elo(session: Session = Depends(get_session)):
    session.query(User) \
        .update({
            User.elo: 1000
        })
    session.commit()
    first_game = session.query(Game) \
        .filter(Game.is_valid) \
        .order_by(Game.date) \
        .first()
    first_game.update_all_elo(session)
    return





# PLZ DON'T PUT THIS IN PRODUCTION IT'S REALLY BAD


# @app.get("/test")
# def test():
#     return user_table
