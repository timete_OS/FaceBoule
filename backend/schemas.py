from datetime import datetime, date
from typing import List, Union
from uuid import uuid4

from pydantic import BaseModel


class UserBase(BaseModel):
    pseudo: Union[str, None] = None
    elo: int = 1000
    id: str = str(uuid4())
    first_name: str
    last_name: str
    member_since: date


class UserCreate(BaseModel):
    email: str
    pswd: str
    first_name: str
    last_name: str
    # find a way to remove id from here


class User(UserBase):
    #items: List[Game] = []
    class Config:
        orm_mode = True


class GameCreate(BaseModel):
    winner: UserBase
    looser: UserBase
    winner_validation: bool = False
    looser_validation: bool = False
    is_valid: bool = False
    comment: Union[str, None] = None
    date: datetime
    fanny: bool = False
    contre: bool = False


class GameBase(GameCreate):
    id: str = str(uuid4())

    winner_elo: int
    looser_elo: int

class Game(GameBase):
    winner_id: str
    looser_id: str


    class Config:
        orm_mode = True


""" 
a = UserBase(**{'first_name' : "Timo",
                'last_name' : "Bidouille",
                'elo': 1,
                'pseudo': '',
                'id' : 1})

b = User(**{'id' : 1,
                'first_name' : "Timo",
                'last_name' : "Bidouille",
                'email' : "timo.bidouille@eleve.ensai.fr",
                'hashed_password' :"l",
                'elo': 1,
                'pseudo': ''})

print(f'a {type(a)}, b {type(b)}')
print(f'b {isinstance(b, User)}, b {isinstance(b, UserBase)}')

 """
