from __future__ import annotations

from sqlalchemy import Boolean, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql import case

from typing import TYPE_CHECKING
# if TYPE_CHECKING:  # Only imports the below statements during type checking

from user import User


from no_app import Base


class Game(Base):
    __tablename__ = "games"

    id = Column(String(36), primary_key=True, index=True)

    winner_id = Column(String, ForeignKey("users.id"), nullable=False)
    looser_id = Column(String, ForeignKey("users.id"), nullable=False)

    winner = relationship("User", foreign_keys=[winner_id], lazy='subquery')
    looser = relationship("User", foreign_keys=[looser_id], lazy='subquery')
    winner_elo = Column(Integer)
    looser_elo = Column(Integer)

    is_valid = Column(Boolean, default=False)
    looser_validation = Column(Boolean, default=False)
    winner_validation = Column(Boolean, default=False)

    fanny = Column(Boolean, default=False)
    contre = Column(Boolean, default=False)

    date = Column(DateTime)
    comment = Column(String)

    def __init__(self, **kwargs):
        super(Game, self).__init__(**kwargs)
        self.winner_elo = kwargs['winner'].elo
        self.looser_elo = kwargs['looser'].elo

    def __repr__(self):
        text = '\n'.join([f"{key} => {attr}" for key,
                         attr in vars(self).items() if key != 'id'])
        return f"GAME {self.id} : \n\n" + text + '\n'
        # return f"Game(id={self.id!r}, winner={self.winner!r}), looser={self.looser!r}"

    def to_json(self):
        return {
            "winner": self.winner,
            "looser": self.looser,
            "fanny": self.fanny,
            "contre": self.contre_fanny,
            "comment": self.comment,
            "date": self.date,
            "looser_elo": self.looser_elo,
            "winner_elo": self.winner_elo
        }


    def update_all_elo(self, session) -> None:
        following_games = session.query(Game).filter(
            (Game.date >= self.date) & (Game.is_valid)
            )

        game_mapper = dict()
        user_mapper = dict()
        iter = 0
        for game in following_games:
            if game.id not in game_mapper:
                game_mapper[game.id] = game
            if game.looser.id not in user_mapper:
                user_mapper[game.looser.id] = game.looser_elo
            if game.winner.id not in user_mapper:
                user_mapper[game.winner.id] = game.winner_elo

            D = abs(user_mapper[game.winner.id] - user_mapper[game.looser.id])
            pD = 1/(1 + 10**(-D/300))
            K = 5  # pool game variance term
            game.winner_elo = user_mapper[game.winner.id]
            game.looser_elo = user_mapper[game.looser.id]

            # en cas de fanny ou de contre
            # user_mapper[game.winner.id] += K*(2 - pD)

            user_mapper[game.winner.id] += K*(1 + game.fanny + 2*game.contre - pD)
            user_mapper[game.looser.id] -= K*(pD + game.fanny + 2*game.contre)

            print(f"LE SCORE DU GAGNANT VARIE DE : {user_mapper[game.winner_id] - game.winner_elo}\n")
            # idéalement on veut faire un truc comme ça, pour ne faire qu'une requete sql par la suite
            # game.update({"winner_elo" : d[game.winner.id]})
            # game.update({"looser_elo" : d[game.looser.id]})
            iter += 1

        # mise à jour des elos dans la base

        session.query(User) \
            .filter(User.id.in_(user_mapper.keys())) \
            .update({
                User.elo: case(
                    user_mapper,
                    value=User.id
                )
            })

        win_elo = {key: value.winner_elo for key, value in game_mapper.items()}
        loose_elo = {key: value.looser_elo for key, value in game_mapper.items()}

        session.query(Game)\
            .filter((Game.date >= self.date) & (Game.is_valid == True))\
            .update({
                Game.winner_elo: case(
                    win_elo,
                    value=Game.id
                ),
                Game.looser_elo: case(
                    loose_elo,
                    value=Game.id
                )
            })
        session.commit()

        # mise à jour des elos dans la user_table
        # for key in user_table:
        #     user_table[key].elo = user_mapper[user_table[key].id]

