from __future__ import annotations
from typing import TYPE_CHECKING
if TYPE_CHECKING:  # Only imports the below statements during type checking
    from game import Game


from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date

from sqlalchemy.orm import relationship

from no_app import Base


class User(Base):
    __tablename__ = "users"

    id = Column(String(36), primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    first_name = Column(String)
    last_name = Column(String)
    pseudo = Column(String, default=None)
    hashed_password = Column(String(256))
    elo = Column(Integer, default=1000)
    member_since = Column(Date)

    tokens = relationship("Token", back_populates="owner")

    """ def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        self.elo = 1 """

    def __repr__(self):
        return f"User(id={self.id!r}, name={self.first_name!r}, surname={self.last_name!r})"

    # mauvaise idée
    # def __eq__(self, __o: object) -> bool:
    #     return self.id == __o.id

    def __hash__(self) -> int:
        return self.id.__hash__()
        
    

class Token(Base):
    __tablename__ = "tokens"

    value = Column(String, primary_key=True)
    owner_id = Column(Integer, ForeignKey("users.id"))

    owner = relationship("User", back_populates="tokens")
