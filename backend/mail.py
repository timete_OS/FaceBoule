import smtplib
import os
import re


from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
from email.mime.base import MIMEBase
from string import Template

from dotenv import load_dotenv
# from user import User

email_checker = re.compile(
    r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')


class Mail:

    def __init__(self) -> None:
        load_dotenv()
        self.PASSWORD = os.environ.get("MAIL_PASSWORD")
        self.MY_ADDRESS = 'timothee@obrecht.xyz'
        message = open('message.txt')
        self.message = message.read()

    def send(self, user, code: str):
        # check if mail is valid
        if not re.fullmatch(email_checker, user.email):
            print("\nNO VALID ADRESS\n")
            return
        # set up the SMTP server
        s = smtplib.SMTP(host='ssl0.ovh.net', port=587)
        s.starttls()
        s.login(self.MY_ADDRESS, self.PASSWORD)

        # create a message
        msg = MIMEMultipart()
        # setup the parameters of the message
        msg['From'] = self.MY_ADDRESS
        msg['To'] = user.email
        msg['Subject'] = "Inscription sur FaceBoule"

        body = Template(self.message).substitute(
            NAME=user.first_name, CODE=code)
        msg.attach(MIMEText(body, 'html'))  # add in the message body
        # add_attachment('carotte.pdf', msg) #add attachment

        s.send_message(msg)  # send the message
        del msg


# si on veut des pieces jointes
def add_attachment(filename, msg):
    attachment = open(filename, "rb")
    part = MIMEBase('application', 'octet-stream')
    part.set_payload((attachment).read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition',
                    "attachment; filename= %s" % filename)
    msg.attach(part)


if __name__ == "__main__":
    from dataclasses import dataclass

    @dataclass
    class Connard:
        """Class for keeping track of an item in inventory."""
        first_name: str
        mail: str
        code: int
        quantity_on_hand: int = 0

    connard = Connard(
        first_name='bibi',
        mail="timothee.obrecht@eleve.ensai.fr",
        code=1234
    )

    mailing = Mail()
    mailing.send(connard)
