FaceBoule Frontend
=========


# How to use it ?

Hopefully, you'll just have to download the app.

I'll try to make it a website in the meantime. You can also just ```npm start```.

# Global architecture

The code for each page is in `src/pages`. You can write your own, it only consist in a function returning HTML with some JS embeded.

In order to link the pages to others, you will need to route it from the `src/js/route.js` file.

There are ready to use component, such as game.jsx and unconfirmed_game.jsx

# Stuff to do


## Aesthetic

- change the font to Montserra
- polish animation between the main pages
- bigger forms, [dedicated input](https://uiinitiative.com/catalog/keypad) for the validation code
- center buttons and inputs
- add boudary to objects, so they don't touch the edges
- ...

## Usability

- fanny & contre doivent pouvoir être déselectionné
- being able to edit first_name and name
- being able to see others profiles
- notify the user if there is anything wrong with game creation or game validation
- ...

## Others

- A working link to this git repo in the login page