import HomePage from '../pages/home.jsx';
import AboutPage from '../pages/about.jsx';
import FormPage from '../pages/form.jsx';
import LeaderboardPage from '../pages/leaderboard.jsx';
import ProfilePage from '../pages/profile.jsx';
import GameItem from '../pages/game-item.jsx';
import LoginPage from '../pages/login.jsx'
import ValidationCodePage from '../pages/validation_code.jsx';
import DynamicRoutePage from '../pages/dynamic-route.jsx';
import RequestAndLoad from '../pages/request-and-load.jsx';
import NotFoundPage from '../pages/404.jsx';
import AllGamesPage from '../pages/all_games.jsx';
import CreateAccountPage from '../pages/create_account.jsx';
import Game from '../pages/game.jsx';
import CompleteRankingPage from '../pages/complete-ranking.jsx';
import CompleteRivalsPage from '../pages/complete-rivals.jsx';
import ChangeInfoPage from '../pages/change-info.jsx';

import { Storage } from '@capacitor/storage';


async function checkAuth (to, from, resolve, reject) {
  const router = this;
  console.log("ici ici ici")
  if (store.getState().auth.isAuthenticated) {
    console.log("oishdoiuzhci")
    resolve()
  } else {
    console.log("yoloyoloyolo")

    reject();
    router.navigate('/login/');
  }
}

var routes = [
  {
    path: '/login/',
    component: LoginPage,
  },
  {
    path: '/about/',
    component: AboutPage,
  },
  {
    path: '/form/',
    component: FormPage,
  },
  {
    path: '/leaderboard/',
    component: LeaderboardPage,
  },
  {
    path: '/profile/',
    component: ProfilePage,
  },
  {
    path: '/game-item/',
    component: GameItem,
  },
  {
    path: '/completeRivals/',
    component: CompleteRivalsPage,
  },
  {
    path: '/game/',
    component: Game,
  },
  {
    path: '/completeRanking/',
    component: CompleteRankingPage,
  },
  {
    path: '/changeInfo',
    component: ChangeInfoPage
  },
  {
    path: '/home/',
    async: async function ({app, to, from, resolve, reject}) {
      
      let token = await Storage.get({ key: 'access_token'})
      console.log('ici le token : ', token.value)
      if (token.value != null) {
        resolve({
          component: HomePage
        });
      } else {
        reject();
        this.navigate('/login/')
      }
    }
  },
  {
    path: '/all_games/',
    component: AllGamesPage,
  },
  {
    path: '/create_account/',
    component: CreateAccountPage
  },
  {
    path: '/validation_code/',
    component: ValidationCodePage
  },




  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: DynamicRoutePage,
  },
  {
    path: '/request-and-load/user/:userId/',
    async: function ({ router, to, resolve }) {
      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = to.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: 'Vladimir',
          lastName: 'Kharlampidi',
          about: 'Hello, i am creator of Framework7! Hope you like it!',
          links: [
            {
              title: 'Framework7 Website',
              url: 'http://framework7.io',
            },
            {
              title: 'Framework7 Forum',
              url: 'http://forum.framework7.io',
            },
          ]
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            component: RequestAndLoad,
          },
          {
            props: {
              user: user,
            }
          }
        );
      }, 1000);
    },
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];

export default routes;
