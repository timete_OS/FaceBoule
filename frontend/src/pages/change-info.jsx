import React from 'react';
import {
  Page,
  Navbar,
  ListInput,
  List,
  NavTitleLarge,
  Button,
  Block
} from 'framework7-react';


import globalStateContext from '../js/global_state';
import { Storage } from '@capacitor/storage';





async function connection(account_info, api_url)
{
    const storage = await Storage.get({key : 'access_token'})
    const token = storage.value
    const params = new URLSearchParams(account_info).toString()

    const rawResponse = await fetch(api_url + "/user?" + params, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'token': token
        }
        });
        const content = await rawResponse.json();
    
        console.log(content);
}



function ChangeInfoPage( {me} )
{
    const {api_url} = React.useContext(globalStateContext);

     
    const [First, setFirst] = React.useState(me.first_name)
    const [Last, setLast] = React.useState(me.last_name)
    const [Pseudo, setPseudo] = React.useState(me.pseudo)


    return (
    <Page name="changeInfo">

        <Navbar large sliding={false} backLink="Back">
            <NavTitleLarge>Modifiez vos informations</NavTitleLarge>
        </Navbar>


        <List inlineLabels noHairlinesMd>

            <ListInput
                label="Prénom"
                type="text"
                placeholder={me.first_name}
                clearButton
                onChange={e=> setFirst(e.target.value)}
                >
            </ListInput>

            <ListInput
                label="Nom"
                type="text"
                placeholder={me.last_name}
                clearButton
                onChange={e=> setLast(e.target.value)}
                >
            </ListInput>


            <ListInput
                label="Pseudo"
                type="text"
                placeholder={me.pseudo != null && me.pseudo}
                clearButton
                onChange={e=> setPseudo(e.target.value)}
                >
            </ListInput>
        </List>

        <Block strong>
            <Button href = '/profile/' outline onClick={
                () => {
                    const account_info = {
                        "first_name" : First,
                        "last_name" : Last,
                        "pseudo" : Pseudo
                    }
                    connection(account_info, api_url)
                }
                }>Confirmer</Button>
        </Block>
    </Page>
    );
}


export default ChangeInfoPage;
