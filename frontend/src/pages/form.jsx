import React from 'react';
import {
  Page,
  Navbar,
  List,
  ListInput,
  ListItem,
  BlockTitle,
  Button,
} from 'framework7-react';

import { Storage } from '@capacitor/storage';

import globalStateContext from '../js/global_state';


// dans cette page on fait des centaines d'appels par seconde
// c'est un probleme

// plus trop maintenant

const event = new Date();

let result = {
                "winner": {
                  "first_name": "string",
                  "last_name": "string",
                  "pseudo": "string",
                  "elo": 0,
                  "id": "string"
                },
                "looser": {
                  "first_name": "string",
                  "last_name": "string",
                  "pseudo": "string",
                  "elo": 0,
                  "id": "string"
                },
                "winner_validation": false,
                "looser_validation": false,
                "is_valid": false,
                "comment": null,
                "date": event.toISOString(),
                "fanny": false,
                "contre": false
              }


function FormPage() {

    const {api_url} = React.useContext(globalStateContext);

    //le paramètre de useState c'est la valeur par défaut des arguments
    const [usersMapper, setMapper] = React.useState({});
    const [token, setToken] = React.useState(''); 


    React.useEffect( () => {
        const call_API = async () => {
            const storage = await Storage.get({key : 'access_token'})
            const token_value = storage.value
            console.log("le token", token_value)
            setToken(token_value)

            const response = await fetch(api_url + '/users')
            console.log("la réponse", response)

            const users = await response.json()

            console.log("les users", users)

            setMapper(Object.assign({}, ...users.map((x) => ({[x.id]: x}) )))
            // setMapper({'1' : {'first_name' : 'timo',
            //                     'last_name' : 'moi'},
            //             '2' : {'first_name' : 'scarlett',
            //                     'last_name' : 'elle'}})
        }

        call_API();
    }, []);
    // très sale : ici je demande de ne pas recharger le state tant que token n'est pas modifié
    // ce qui n'arrice jamais naturellement
    // simplement parce que je ne sais pas quel truc modifie l'etat dans les lignes suivantes


    return (
        <Page name="form">
            <Navbar title="Enregistrer une nouvelle partie" backLink="Back"></Navbar>

            <List noHairlinesMd>

            <ListItem
                title="Gagnant"
                smartSelect
                smartSelectParams={{
                openIn: 'popup',
                searchbar: true,
                closeOnSelect: true,
                searchbarPlaceholder: 'chercher un.e joueureuse',
                }}
                >
                <select
                    name="Gagnant" 
                    defaultValue={''}
                    onChange={e=> 
                            {
                                result["winner"] = usersMapper[e.target.value]
                            }}>
                    <option></option>
                    {/* si jamais on veut ne prendre qu'un unique attribut de ce qui a été séléctioné */}
                    {Object.entries(usersMapper).map(element => <option value = {element[0]}>{element[1].first_name} {element[1].last_name}</option>)}
                    {/* {users.map(element => <option>{element.first_name} {element.last_name}</option>)} */}
                </select>
            </ListItem>


            <ListItem
                title="Perdant"
                smartSelect
                smartSelectParams={{
                openIn: 'popup',
                searchbar: true,
                closeOnSelect: true,
                searchbarPlaceholder: 'chercher un.e joueureuse',
                }}
                >
                <select
                    name="Perdant"
                    defaultValue={''}
                    onChange={e=> 
                        {
                            console.log(e.target.value)
                            console.log("le mapper est quoi ?", usersMapper)
                            result["looser"] = usersMapper[e.target.value]
                            console.log(result["looser"])}
                        }>
                    <option></option>
                    {/* ici on boucle sur la liste des utilisateurs */}
                    {Object.entries(usersMapper).map(element => <option value = {element[0]}>{element[1].first_name} {element[1].last_name}</option>)}
                </select>
            </ListItem>

      

            <ListInput
                type="textarea"
                label="Commentaire (facultatif)"
                placeholder="Cette partie était une honte absolue ! Que de la chatte !"
                onInputNotEmpty={e=> result["comment"] = e.target.value}
                resizable
            ></ListInput>
            </List>


            <BlockTitle>Facultatif : Particularités de la partie</BlockTitle>
            <List>
            <ListItem
                radio
                name="my-checkbox"
                value="fanny"
                title="Fanny"
                onChange={e=> result["fanny"] = (e.target.value == "fanny")}
                >
            </ListItem>
            <ListItem
                radio
                name="my-checkbox"
                value="contre-fanny"
                title="Contre-Fanny"
                onChange={e=> result["contre_fanny"] = (e.target.value == "contre_fanny")}
                >
            </ListItem>
            </List>


            <Button className="col" href = "/home/" onClick={
                (async () => {
                    console.log(JSON.stringify(result))

                    console.log("le token lors de la requete : ", token)

                    var game = new FormData();
                    game.append( "json", JSON.stringify( result ) );
                    if (!((result['winner']['id'] == '') | (result['looser']['id'] == '')))
                    {
                        const rawResponse = await fetch(api_url + "/game", {
                        method: 'PUT',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            'token': token
                        },
                        body: JSON.stringify( result )

                        });
                        const content = await rawResponse.json();
                    
                        console.log(content);
                    }
                })
            }
            round outline>Sauvegarder la partie</Button>
          
        </Page>

);
}

export default FormPage;



// <List noHairlinesMd>

// <ListInput
//     label="Perdant"
//     type="select"
//     users-searchbar="true"
//     onInputNotEmpty={e=> 
//         {
//             console.log(usersMapper)
//             result["looser"] = usersMapper[e.target.value]
//             console.log(result["looser"])}
//         }
//     >
//         <option></option>
//         {/* ici on boucle sur la liste des utilisateurs */}
//         {Object.entries(usersMapper).map(element => <option value = {element[0]}>{element[1].first_name} {element[1].last_name}</option>)}
// </ListInput>

// <ListInput
//     label="Gagnant"
//     type="select"
//     users-searchbar="true"
//     onInputNotEmpty={e=> result["winner"] = usersMapper[e.target.value]}
//     >
//         <option></option>
//         {/* si jamais on veut ne prendre qu'un unique attribut de ce qui a été séléctioné */}
//         {Object.entries(usersMapper).map(element => <option value = {element[0]}>{element[1].first_name} {element[1].last_name}</option>)}
//         {/* {users.map(element => <option>{element.first_name} {element.last_name}</option>)} */}
// </ListInput>

// <ListInput
//     type="textarea"
//     label="Commentaire (facultatif)"
//     placeholder="Cette partie était une honte absolue ! Que de la chatte !"
//     onInputNotEmpty={e=> result["comment"] = e.target.value}
//     resizable
// ></ListInput>
// </List>


// <BlockTitle>Facultatif : Particularités de la partie</BlockTitle>
// <List>
// <ListItem
//     radio
//     name="my-checkbox"
//     value="fanny"
//     title="Fanny"
//     onChange={e=> result["fanny"] = (e.target.value == "fanny")}
//     >
// </ListItem>
// <ListItem
//     radio
//     name="my-checkbox"
//     value="contre-fanny"
//     title="Contre-Fanny"
//     onChange={e=> result["contre_fanny"] = (e.target.value == "contre_fanny")}
//     >
// </ListItem>
// </List>


// <Button className="col" href = "/home/" onClick={
//     (async () => {
//         console.log("le mapper : ", usersMapper);
//         console.log(JSON.stringify(result))

//         console.log("le token lors de la requete : ", token)

//         var game = new FormData();
//         game.append( "json", JSON.stringify( result ) );
//         if (!((result['winner']['id'] == '') | (result['looser']['id'] == '')))
//         {
//             const rawResponse = await fetch(api_url + "/game", {
//             method: 'PUT',
//             headers: {
//                 'Accept': 'application/json',
//                 'Content-Type': 'application/json',
//                 'token': token
//             },
//             body: game
//             });
//             const content = await rawResponse.json();
        
//             console.log(content);
//         }
//     })
// }
// round outline>Sauvegarder la partie</Button> 