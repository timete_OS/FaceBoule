import React from 'react';
import {
  Page,
  Navbar,
  NavTitle,
  NavTitleLarge,
  Button,
  Block,
  List,
  ListInput
} from 'framework7-react';

import { Storage } from '@capacitor/storage';

import globalStateContext from '../js/global_state';

const response = {
  mail: '',
  code: ''
}


async function connection(response, api_url)
{
    const params = new URLSearchParams(response).toString()
    console.log(params)
    const rawResponse = await fetch(api_url + "/user/email_confirmation?" + params, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        });
        const content = await rawResponse.json();
        
        console.log(content);
        return content
    }

function ValidationCodePage( {f7router, mail})
{
    const {api_url} = React.useContext(globalStateContext);
    response.mail = mail
    console.log("GLOUPI", response.mail)

    const blabla = async() => {
        const prout = await connection(response, api_url);

        if (prout.access_token != null)
        {
            console.log("youpi")
            await Storage.set({
                key: 'access_token',
                value: prout.access_token,
            });
            await Storage.set({
                key: 'id',
                value: prout.id,
            });
            f7router.navigate('/home/');
        }
        else
        {         
            f7.dialog.alert("Le code est invalide", () => {
            f7router.back(); 
        });
        }
    
    }

    return (
    <Page name="validation_code">

        <Navbar large sliding={true} backLink="Back">
            <NavTitle>Confirmation de l'email</NavTitle>
        </Navbar>

        <List>

            <ListInput
            centered={true}
            type="text"
            placeholder="Code de validation"
            required validate pattern="[0-9]*"
            data-error-message="Only numbers please!" 
            onInput={(e) => {
                response.code = e.target.value;
            }}/>
        </List>



        <Block strong>
            <Button outline onClick={blabla}>Valider</Button>
        </Block>
    </Page>
    );
}


export default ValidationCodePage;
