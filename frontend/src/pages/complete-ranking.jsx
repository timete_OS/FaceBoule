import React from 'react';
import {
  Page,
  Navbar,
  List,
  Block,
  ListItem,
  BlockTitle,
} from 'framework7-react';


function CompleteRankingPage(props) {

    // console.log(props)
    // console.log(this.state.users)
    const users = props.user_list
    console.log(users)

    return (
        <Page name="completeRanking">
            <Navbar title="ELO World" backLink="Back"></Navbar>

        <Block>
            <List>
                {users.sort(function (a, b) {
                            return b.elo - a.elo;})
                .map(element => <ListItem title= {element.first_name} footer={element.last_name} after = {element.elo}/>)}
            </List>
        </Block>

        </Page>

);
}

export default CompleteRankingPage;

