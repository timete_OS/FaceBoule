import React from 'react';
import {
  Page,
  Card,
  List,
  CardContent,
  Toolbar,
  Link,
  Tab,
  Tabs,
  NavTitle,
  Block,
  Navbar,
  NavTitleLarge,
  Button,
  ListInput
} from 'framework7-react';


import globalStateContext from '../js/global_state';

import UnconfirmedGameItem from './unconfirmed_game';

import { Storage } from '@capacitor/storage';



function ProfilePage ()  {

    const {api_url} = React.useContext(globalStateContext);


    //le paramètre de useState c'est la valeur par défaut des arguments

    const [nameField, setNameField] = React.useState('');
    const [token, setToken] = React.useState(''); 
    const [me, setMe] = React.useState({});
    const [unconfirmed_games, setUnconfirmed_games] = React.useState([]); 

    React.useEffect(() => {   
        async function getEverything() {
            // You can await here
            const storage = await Storage.get({key : 'access_token'})
            const token = storage.value
            console.log("le token", token)
            setToken(token)

            const me = await fetch(api_url + "/who_am_I", {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'token': token
                }
                }).then(res => res.json())

            setMe(me)
            console.log("moi", me)
            console.log("mon id", me.id)


            if (me.pseudo != null)
            {
                setNameField(me["first_name"] + ' ' + me["last_name"] + " aka. " + me.pseudo)
            }
            else
            {
                setNameField(me["first_name"] + ' ' + me["last_name"])
            }
            fetch(api_url + "/games/" + me.id +  "/unconfirmed", {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
                })
            .then(unconfirmed_games => unconfirmed_games.json()
            .then(json => setUnconfirmed_games(json)))

            console.log(unconfirmed_games)

            }

        
        getEverything();
    }, []);


    function removeGame(id) {
        let filteredArray = unconfirmed_games.filter(item => item.id !== id)
        setUnconfirmed_games(filteredArray);
    }

    return (
        <Page name="Profil">
        <Navbar large sliding={false}>
          <NavTitle sliding>Mon profil</NavTitle>
          <NavTitleLarge>Mon profil</NavTitleLarge>
        </Navbar>

        <Toolbar tabbar bottom>
        <Link tabLink="#home" href="/home/">Home</Link>
        <Link tabLink="#leaderboard" href="/leaderboard/">Classement</Link>
        <Link tabLink="#profile" tabLinkActive>Profil</Link>
        </Toolbar>


        <Tabs animated>

        <Tab id="profile" className="page-content">

        <Block>
        <Card
          title= {
            nameField
        }
          content={"Mon ELO : " + me["elo"]}>
            <Button center href = '/changeInfo/' routeProps={{'me': me}} >Modifier vos informations</Button>
 
            </Card>
        </Block>



        <Card title={unconfirmed_games.length != 0 && "Les parties à confirmer :"}>
            <CardContent padding={false}>
                <List medial-list>
                    {unconfirmed_games.
                    sort(function (a, b) {return new Date(b.date) - new Date(a.date);}).
                    map(element => <UnconfirmedGameItem game = {element} user = {me} token = {token} del = {removeGame}/>)}
                </List>
            </CardContent>
  
        </Card>
        <center>
        <h3>{unconfirmed_games.length == 0 && "Vous n'avez aucune partie à confirmer"}</h3>



        <Button outline href="/login/" onClick={
            async () => { await Storage.remove({ key: 'access_token' }) }}
            >Se déconnecter</Button>


        <div>
            Contribuer au developpement sur <a class="link external"  target="_blank" href="https://gitlab.com/timete_OS/FaceBoule">GitLab</a>
        </div>
        </center>
        </Tab>
        </Tabs>
        </Page>
    );
    }

export default ProfilePage;
