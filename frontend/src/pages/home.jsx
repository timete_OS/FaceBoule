import React from 'react';
import {
  Page,
  Navbar,
  Button,
  Tabs,
  Tab,
  Toolbar,
  Link,
  NavTitle,
  NavTitleLarge,
  Card,
  CardContent,
  CardFooter,
  List,
} from 'framework7-react';


import globalStateContext from '../js/global_state';
import GameItem from './game-item';


function HomePage() {

    const {api_url} = React.useContext(globalStateContext);

    const [data, setData] = React.useState([]); //le paramètre de useState c'est la valeur par défaut des arguments
    React.useEffect(() => {fetch(api_url + '/games?limit=9')
    .then(data => data.json()
    .then(json => setData(json)))}, []);
    /*const checkName = async () => {
        const { value } = await Storage.get({ key: 'access_token' });
      };
    React.useEffect(() => {checkName()
    .then(data => setData(data))
    .then(data => console.log(data))});
    console.log('url : ' , api_url)*/

    return (

        /* 
        pour faire les appels à des API et avoir des variable d'environnement
        React.useEffect(() => )

        */ 
        <Page name="home">
        {/* Top Navbar */}
        <Navbar large sliding={true}>
            <NavTitle sliding>FaceBoule</NavTitle>
            <NavTitleLarge>FaceBoule</NavTitleLarge>
        </Navbar>



        <Toolbar tabbar bottom>
            <Link tabLink="#home" tabLinkActive>Home</Link>
            <Link tabLink="#leaderboard"  href="/leaderboard/">Classement</Link>
            <Link tabLink="#profile" href="/profile/">Profil</Link>
        </Toolbar>



        <Tabs animated>
            <Tab id="home" className="page-content" tabActive>
                <Button href = "/form/" data-transition="f7-cover-v" outline>Nouvelle partie</Button>
                <Card title="Parties récentes :">
                <CardContent padding={false}>
                    <List medial-list>
                        {data.
                        sort(function (a, b) {return new Date(b.date) - new Date(a.date);}).
                        map(element => GameItem(element))}
                    </List>
                </CardContent>
                <CardFooter>
                    <a href = "/all_games/"><span>Voir toutes les parties</span></a> {/* ici un lien vers un liste de toutes les parties*/}
                </CardFooter>
                </Card>

            </Tab>
        </Tabs> 

        </Page>
);
}



export default HomePage;