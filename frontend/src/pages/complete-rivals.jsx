import React from 'react';
import {
  Page,
  Navbar,
  List,
  Block,
  ListItem,
} from 'framework7-react';


function CompleteRivalsPage(props) {

    const rivals = props.rival_list
    console.log(rivals)
    return (
        <Page name="completeRanking">
            <Navbar title="Vue d'ensemble des rivaux" backLink="Back"></Navbar>

        <Block>
            <List>
                {rivals.sort(function (a, b) {
                            return b.total_games - a.total_games;})
                .map(element => <ListItem title= {element.first_name} footer={element.last_name}>
                    {(element.victory ).toString()} victoires contre {(element.defeat).toString()} défaites
                </ListItem>)}
            </List>
        </Block>

        </Page>

);
}

export default CompleteRivalsPage;
