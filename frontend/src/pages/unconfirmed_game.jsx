import React from 'react';
import {
  Page,
  Navbar,
  List,
  ListInput,
  ListItem,
  Toggle,
  Block,
  Row,
  Col,
  Button,
} from 'framework7-react';

import globalStateContext from '../js/global_state';


async function send_validation(api_url, token, game_id, validation)
{
    console.log("validation", validation)
    console.log("game id", game_id)
    console.log("token", token)

    const params = new URLSearchParams({'validation' : validation}).toString()

    console.log('params  : ', params)
    const rawResponse = await fetch(
        api_url + "/game/" + game_id + "/validation/?" + params ,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'token': token
                },
                // body: JSON.stringify({'validation' : validation})
/*                 body : new URLSearchParams({
                    'game_id': game_id,
                    'validation': validation
                }).toString() */
            }
        );
    const content = await rawResponse.json();
    console.log(content);
};

function Winning_or_loosing(props)
{
    const d = new Date(props.game['date'])
    if (props.game.winner.id == props.user.id)
    {
        return (
        <div>
        Vous avez <strong>gagné</strong> contre
        <br/>
        {props.game['looser']['first_name']} {props.game['looser']['last_name']}
        <i> le {d.toLocaleString()}</i>
        </div>
        );
    }
    return (
    <div>
    Vous avez <strong>perdu</strong> contre
    <br/>
    {props.game['winner']['first_name']} {props.game['winner']['last_name']}
    <i> le : {d.toLocaleString()}</i>
    </div>
    );
}


function UnconfirmedGameItem(props) {

    const {api_url} = React.useContext(globalStateContext);


    return (
        <Block>
            {console.log(props.game)}
        <ListItem>
            <Winning_or_loosing game={props.game} user={props.user}/>
        </ListItem>
            <Row>
                <Col width="50">
                    <Button outline round onClick={() => {
                        send_validation(api_url, props.token, props.game["id"], true)
                        props.del(props.game.id)
                    }
                    // détruire le composant, ou bien recharger la page
                    
                    }>Accepter</Button>
                </Col>
                <Col width="50">
                    <Button outline round onClick={() => {
                        send_validation(api_url, props.token, props.game["id"], false);
                        props.del(props.game.id)     
                    }
                    // recharger la page
                    }>Rejeter</Button>
                </Col>
            </Row>
        </Block>
    );
}


export default UnconfirmedGameItem;
                
            