FROM python:3-alpine as builder

RUN apk update


# RUN apk add python3
# RUN apk add py3-pip

# RUN apk add python3 &&\
    # apk add py3-pip &&\
RUN  mkdir faceboule

# /home/timothee/Documents/FaceBoule/backend
COPY backend/ faceboule/


RUN pip install -r faceboule/requirements.txt


WORKDIR /faceboule

ENTRYPOINT ["python3", "no_main.py"]

FROM alpine:latest

RUN  mkdir faceboule
    
COPY backend/ faceboule/

COPY --from=builder /usr/local/bin/python3.11 /usr/local/bin/python3.11
COPY --from=builder /usr/local/bin/uvicorn /usr/local/bin/uvicorn
COPY --from=builder /usr/local/bin/dotenv /usr/local/bin/dotenv

ENV PATH="$PATH:/usr/local/bin/python3.11"

WORKDIR /faceboule

EXPOSE 8000

ENTRYPOINT ["python3", "no_main.py"]

# docker build . -t faceboule
# docker run -p 8000:8000 faceboule