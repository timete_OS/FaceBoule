FaceBoule
=====================

## C'est quoi FaceBoule ?

Un app simple qui permet de tenir le compte des parties de billard de l'ENSAI. À la fin de chaque partie, les joueurs peuvent simplement en enregistrer l'issue. Ainsi on peut maintenir un classement ELO des élèves de l'école. Il faut avoir un mail @ensai pour s'inscrire.

## À quoi ça ressemble


![image](screenshots/global.png)

L'interface se doit de rester le plus simple possible. Trois espaces suffisent :

* Un pour créer et consulter des parties.
* Un pour accéder au classement et aux pages des utilisateurs.
* Un pour consulter et modifier ses informations personelles, et éventuellement valider des parties.

Si des âmes courageuses le veulent, elle pourront implémenter un système de commentaires ou de notifications, pour que chacun puisse se moquer des défaites de ses amis. Quoique que je doute de la pertinence de cette idée.

L'implémentation des matchs de double ne sera pas particulièrement complexe.

## Détails sur le classement

Un classement ELO repose sur une formule de probabilité sur le fond assez simple. Les variations de points dépendent d'une probabilité estimée de victoire/défaite. Cette probabilité dépend de la **variance** inhérente au jeu. Ce facteur est inconnu.

Il va donc falloir l'estimer au fur et à mesure des parties. Un joli sujet de projet stat *imho*.

## Détails sur l'implémentation de tout ça

Il s'agit d'une web app des plus moches et des plus classiques.

L'interface est codé en React, en utilisant Framework7. Elle communique avec une l'API de **faceboule.fr** (que vous pouvez parfaitement appeler vous-même) hébergée sur un micro-serveur AWS, qui tourne avec FastAPI et une base de données SQLite.

Des détails sur le front et le back sont donnés dans leurs dossier respectifs.

## Contribution & Futur

Je suis une grosse quiche en JavaScript, et en *frontend* de façon générale. J'encourage quiconque avec une vague expérience en web à contribuer, vous ferez mieux que moi.


Le futur de ce projet est pour l'instant incertain. Inch'Allah ça marche et les gens aiment.